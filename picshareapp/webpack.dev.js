import webpack from 'webpack';
import merge from 'webpack-merge';
import { BundleAnalyzerPlugin } from 'webpack-bundle-analyzer';

import config, { distDir } from './webpack.common.js';

export default merge(config, {
	mode: 'development',
	devtool: 'cheap-module-source-map',
	devServer: {
		contentBase: distDir,
		historyApiFallback: true,
		hot: true,
		host: process.env.DEV_SERVER_HOST || 'localhost',
		port: process.env.DEV_SERVER_PORT || 3000,
		publicPath: '/',
		watchOptions: {
			ignored: /node_modules/
		}
	},
	plugins: [
		new webpack.NamedModulesPlugin(),
		new webpack.HotModuleReplacementPlugin(),
		new BundleAnalyzerPlugin({
			analyzerPort: process.env.BUNDLE_ANALYZER_PORT || 8888,
			openAnalyzer: false
		})
	],
	performance: {
		maxEntrypointSize: 1024000,
		maxAssetSize: 1024000,
		hints: 'warning'
	}
});
