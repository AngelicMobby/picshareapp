import { resolve } from 'path';
import { createMissingDotEnvFile } from './utils/dotenv';

const CONFIG_FILEPATH = resolve(__dirname, '../.env');
const CONFIG_EXAMPLE_FILEPATH = resolve(__dirname, '../.env.example');

createMissingDotEnvFile(CONFIG_FILEPATH, CONFIG_EXAMPLE_FILEPATH);
