import React from 'react';
import { any, arrayOf, object, oneOf, oneOfType, number, string } from 'prop-types';

import spacing from '~tokens/spacing.mjs';
import breakpoints from '~src/helpers/breakpoints.mjs';
const spaces = Object.keys(spacing);

import { GridStyled } from './GridStyled';

const Grid = ({ children, childPadding, ...props }) => (
	<GridStyled className="Grid" {...props}>
		{React.Children.map(children, child =>
			React.cloneElement(child, {
				padding: childPadding
			})
		)}
	</GridStyled>
);

Grid.propTypes = {
	align: oneOf(['stretch', 'flex-start', 'center']),
	childPadding: string,
	columnGap: oneOfType([oneOf([...spaces]), number]),
	columns: oneOfType([arrayOf(object), number, oneOf(['auto-fit', 'auto-fill'])]),
	children: any /* eslint-disable-line react/forbid-prop-types */,
	margins: string,
	maxWidth: string,
	minColumnWidth: string,
	rowGap: oneOfType([oneOf([...spaces]), number])
};

Grid.defaultProps = {
	align: 'stretch',
	childPadding: '',
	columnGap: 40,
	columns: [{ columns: 4 }, { break: breakpoints.maxMobile, columns: 6 }],
	margins: '0 auto',
	maxWidth: `${breakpoints.maxPageWidth}px`,
	rowGap: 0
};

export default Grid;
