import React from 'react';
import { bool, string } from 'prop-types';

import Label from '~src/components/UI/Label';

import { InputWrapperStyled, InputStyled } from './InputStyled';

const Input = ({ name, label, ...props }) => (
	<InputWrapperStyled>
		<Label htmlFor={name}>{label}</Label>
		<InputStyled id={name} name={name} {...props} />
	</InputWrapperStyled>
);

Input.propTypes = {
	type: string,
	label: string,
	name: string.isRequired,
	required: bool,
	inline: bool
};

Input.defaultProps = {
	type: 'text',
	label: 'Input',
	required: false,
	inline: false
};

export default Input;
