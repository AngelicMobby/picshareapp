import styled from 'styled-components';

import colors from '~tokens/colors.mjs';
import spacing from '~tokens/spacing.mjs';
import fontSizes from '~tokens/fontSizes.mjs';
import lineHeights from '~tokens/lineHeights.mjs';

import breakpoints from '~src/helpers/breakpoints.mjs';

export const InputWrapperStyled = styled.div`
	width: 100%;
	box-sizing: border-box;
	margin-bottom: ${spacing.small};
	font-size: 16px;
`;

export const InputStyled = styled.input`
	appearance: none;
	width: ${({ inline }) => inline ? 'auto' : '100%'};
	box-sizing: border-box;
	margin-top: ${spacing.tiny};
	padding: ${spacing.tiny};
	font-size: ${fontSizes.body1};
	line-height: 1;
	border: 0;
	border-radius: 5px;
	border-bottom: 1px solid ${colors.gray3};
	background-color: ${colors.gray5};

	&:focus {
		outline: 0;
	}

	@media screen and (max-width: ${breakpoints.breakpointM}px) {
		font-size: 16px;
	}
`;
