import React from 'react';
import { bool } from 'prop-types';

import { H3Styled } from './H3Styled';

const H3 = props => <H3Styled {...props} />;

H3.propTypes = {
	center: bool
};

H3.defaultProps = {
	center: false
};

export default H3;
