import React from 'react';

import { OverlayStyled } from './OverlayStyled';

const Overlay = props => <OverlayStyled {...props} />;

Overlay.propTypes = {};

Overlay.defaultProps = {};

export default Overlay;
