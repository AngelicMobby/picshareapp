import React from 'react';
import { bool, object, string } from 'prop-types';

import createImageUrl from '~src/helpers/createImageUrl.mjs';
import breakpoints from '~src/helpers/breakpoints.mjs';

import { ImageWrapperStyled, ImageStyled } from './ImageStyled';

const sizeFull = 100;

const Image = ({ src, caption, sizes, responsive, width }) => {
	const imgCaption = caption || 'Image caption';

	return responsive
		? (
			<ImageWrapperStyled>
				<ImageStyled
					data-srcset={`
						${createImageUrl(src, 'jpg', 2880, 30)} 2880w,
						${createImageUrl(src, 'jpg', 1920, 30)} 1920w,
						${createImageUrl(src, 'jpg', 1440, 30)} 1440w,
						${createImageUrl(src, 'jpg', 1024, 30)} 1024w,
						${createImageUrl(src, 'jpg', 768, 30)} 768w,
						${createImageUrl(src, 'jpg', 480, 30)} 480w,
						${createImageUrl(src, 'jpg', 320, 30)} 320w`}
					data-sizes={`
						(max-width: ${breakpoints.breakpointsXs}px) ${sizes.xs ? sizes.s : sizeFull}%,
						(max-width: ${breakpoints.breakpointsS}px) ${sizes.s ? sizes.s : sizeFull}%,
						(max-width: ${breakpoints.breakpointsM}px) ${sizes.m ? sizes.m : sizeFull}%,
						(max-width: ${breakpoints.breakpointsL}px) ${sizes.l ? sizes.l : sizeFull}%,
						(max-width: ${breakpoints.breakpointsXl}px) ${sizes.xl ? sizes.xl : sizeFull}%,
					`}
					src="data:image/gif;base64,R0lGODlhAQABAAAAACH5BAEKAAEALAAAAAABAAEAAAICTAEAOw=="
					alt={imgCaption}
					title={imgCaption}
					maxWidth
					className="lazyload"
				/>
			</ImageWrapperStyled>
		) : (
			<ImageStyled
				data-src={src}
				src="data:image/gif;base64,R0lGODlhAQABAAAAACH5BAEKAAEALAAAAAABAAEAAAICTAEAOw=="
				alt={imgCaption}
				title={imgCaption}
				className="lazyload"
			/>
		);
};

Image.propTypes = {
	src: string.isRequired,
	caption: string,
	sizes: object,
	width: string,
	responsive: bool
};

Image.defaultProps = {
	caption: '',
	width: '100%',
	sizes: {},
	responsive: false
};

export default Image;
