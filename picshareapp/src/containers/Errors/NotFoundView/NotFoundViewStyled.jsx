import styled from 'styled-components';

import spacing from '~tokens/spacing.mjs';

export const SectionStyled = styled.section`
	width: 100%;
	padding: ${spacing.medium};
	text-align: center;
`;
